<?php

namespace Planet17\MessageQueueProcessManager\DTO;

/**
 * Class ProcessManagerDTO
 *
 * @package Planet17\MessageQueueProcessManager\DTO
 */
class ProcessManagerDTO
{
    /** @var string */
    private $alias;

    /** @var int */
    private $numbers;

    /**
     * ProcessManagerDTO constructor.
     *
     * @param string $alias
     * @param int $numbers
     */
    public function __construct(string $alias, int $numbers)
    {
        $this->alias = $alias;
        $this->numbers = $numbers;
    }

    /**
     * @return string
     */
    public function getRouteAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return int
     */
    public function getNumbers(): int
    {
        return $this->numbers;
    }
}
