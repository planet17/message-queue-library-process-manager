<?php

namespace Planet17\MessageQueueProcessManager\Repositories;

use Planet17\MessageQueueProcessManager\Interfaces\Repositories\InitializedHandlerProcessesRepositoryInterfaces;

/**
 * Class InitializedHandlersInMemoryRepository
 *
 * @package Planet17\MessageQueueProcessManager\Repositories
 */
class InitializedHandlerProcessesInMemoryRepository implements InitializedHandlerProcessesRepositoryInterfaces
{
    /** @var array $registered Initialized handler PIDs. */
    private $registered;

    /** @inheritdoc  */
    public function add($alias, $pid): InitializedHandlerProcessesRepositoryInterfaces
    {
        $this->registered[$alias][] = $pid;

        return $this;
    }

    /** @inheritdoc  */
    public function getAll($alias): array
    {
        return $this->registered[$alias] ?? [];
    }

    /** @inheritdoc  */
    public function getOne($alias): ?int
    {
        return $this->registered[$alias][array_key_first($this->registered[$alias])];
    }

    /** @inheritdoc  */
    public function remove($alias, $pid): InitializedHandlerProcessesRepositoryInterfaces
    {
        unset($this->registered[$alias][array_search($this->registered[$alias], $pid, true)]);

        return $this;
    }

    /** @inheritdoc  */
    public function count($alias): int
    {
        return count($this->registered[$alias] ?? []);
    }

    /** @inheritdoc  */
    public function removeAll($alias): InitializedHandlerProcessesRepositoryInterfaces
    {
        $this->registered[$alias] = [];

        return $this;
    }

    /**
     * Shift an PID element from array.
     *
     * @param string $alias
     *
     * @return int|null
     */
    public function extract(string $alias): ?int
    {
        return array_shift($this->registered[$alias]);
    }

    private function ensureAliasExist(string $alias)
    {
        if (!array_key_exists($alias, $this->registered)) {

        }
    }
}
