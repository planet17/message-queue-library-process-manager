<?php

namespace Planet17\MessageQueueProcessManager\Routes;

use Planet17\MessageQueueLibrary\Routes\ConnectableRoute;

/**
 * Class ProcessManagerRoute
 *
 * @package Planet17\MessageQueueProcessManager\Routes
 */
class ProcessManagerRoute extends ConnectableRoute
{
    /** @const ALIAS */
    public const ALIAS = 'process-manager';

    /** @inheritdoc  */
    public function getAliasShort(): string
    {
        return self::ALIAS;
    }
}
