<?php

namespace Planet17\MessageQueueProcessManager\Messages;

use Planet17\MessageQueueLibrary\Messages\BaseMessage;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;
use Planet17\MessageQueueProcessManager\Exception\WrongDTOProvidedException;
use Planet17\MessageQueueProcessManager\Routes\ProcessManagerRoute;

/**
 * Class ProcessManagerMessage
 *
 * @package Planet17\MessageQueueProcessManager\Messages
 */
class ProcessManagerMessage extends BaseMessage
{
    protected $routeClass = ProcessManagerRoute::class;

    /** @var ProcessManagerDTO */
    private $dto;

    /**
     * Message constructor.
     *
     * @param null|ProcessManagerDTO $payload
     *
     * @throws WrongDTOProvidedException
     *
     * @noinspection MagicMethodsValidityInspection
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct($payload = null)
    {
        if (!($payload instanceof ProcessManagerDTO)) {
            throw new WrongDTOProvidedException;
        }

        $this->dto = $payload;
    }

    /**
     * Override.
     *
     * @return string
     */
    public function getPayload(): string
    {
        return serialize($this);
    }

    /**
     * Method return route alias what message contains.
     *
     * @return string
     */
    public function getPayloadAlias(): string
    {
        return $this->dto->getRouteAlias();
    }

    /**
     * Method return number of handlers what message contains.
     *
     * @return int
     */
    public function getPayloadNumber(): int
    {
        return $this->dto->getNumbers();
    }
}
