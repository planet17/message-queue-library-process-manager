<?php

namespace Planet17\MessageQueueProcessManager\Exception;

use InvalidArgumentException;
use Throwable;

/**
 * Class WrongDTOProvidedException
 *
 * @package Planet17\MessageQueueProcessManager\Exception
 */
class WrongDTOProvidedException extends InvalidArgumentException
{
    /** @const DEFAULT_MESSAGE */
    public const DEFAULT_MSG = 'Wrong DTO class provided';

    /**
     * WrongDTOProvidedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MSG, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
