<?php

namespace Planet17\MessageQueueProcessManager\Handlers;

use Exception;
use Planet17\MessageQueueLibrary\Handlers\BaseHandler;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibraryRouteNav\Interfaces\Connections\ManagerInterface;
use Planet17\MessageQueueProcessManager\DTO\ProcessManagerDTO;
use Planet17\MessageQueueProcessManager\Interfaces\Repositories\InitializedHandlerProcessesRepositoryInterfaces;
use Planet17\MessageQueueProcessManager\Messages\ProcessManagerMessage;
use Planet17\MessageQueueProcessManager\Repositories\InitializedHandlerProcessesInMemoryRepository;
use Planet17\MessageQueueProcessManager\Routes\ProcessManagerRoute;

/**
 * Class ProcessManagerHandler
 *
 * @package Planet17\MessageQueueProcessManager\Handlers
 */
abstract class ProcessManagerHandler extends BaseHandler
{
    protected $routeClass = ProcessManagerRoute::class;

    /** @var InitializedHandlerProcessesRepositoryInterfaces $repository */
    protected $repository;

    /**
     * Override and add call custom methods.
     */
    public function initialize(): void
    {
        $this->configureRepositoryClasses();
        $this->initializeRegisteredHandlers();
        parent::initialize();
    }

    /**
     * Method encapsulated creation of new process and return `pid`.
     *
     * @param HandlerInterface $handler
     *
     * @return int Process ID (pid).
     */
    abstract protected function makeNewProcess(HandlerInterface $handler): int;

    /**
     * @param int $pid
     */
    protected function killProcess(int $pid): void
    {
        posix_kill($pid, SIGKILL);
    }

    /**
     * Implement return instance of concrete main Connection Manager.
     *
     * @return ManagerInterface
     */
    abstract protected function getConnectionManager(): ManagerInterface;

    /**
     * Implement create new instance of concrete Message with command wrapping for running provided dto as payload.
     *
     * @param ProcessManagerDTO $dto
     *
     * @return MessageInterface
     */
    abstract protected function makeSelfMessage(ProcessManagerDTO $dto): MessageInterface;

    /**
     * Method set up concrete class instance of repository to property.
     *
     * Override it whether you need.
     */
    protected function configureRepositoryClasses(): void
    {
        $this->repository = new InitializedHandlerProcessesInMemoryRepository;
    }

    /**
     * Method return repository with registered initialized handlers.
     *
     * @return InitializedHandlerProcessesRepositoryInterfaces
     */
    private function getRepository():InitializedHandlerProcessesRepositoryInterfaces
    {
        return $this->repository;
    }

    /**
     * Method for init all instances of handlers.
     *
     * It get all handlers and then add message(s) to queue with handler name.
     */
    protected function initializeRegisteredHandlers(): void
    {
        $registered = $this->getConnectionManager()->getRoutesProvider()->getMapped();
        foreach ($registered as $alias => $item) {
            if ($alias === $this->getRoute()->getAliasFull()) {
                continue;
            }

            $this->forgeNewMessageToSelf($alias, 1);
        }
    }

    /**
     * Method for put message to the queue with handler alias for initializing.
     *
     * @param string $routeAlias
     * @param int $instanceNumbers
     */
    private function forgeNewMessageToSelf(string $routeAlias, int $instanceNumbers): void
    {
        $dto = new ProcessManagerDTO($routeAlias, $instanceNumbers);
        $this->makeSelfMessage($dto)->dispatch();
    }

    /**
     * Method handle incoming message and forge new instance of handler.
     *
     * @param ProcessManagerMessage $payload
     *
     * @inheritdoc
     * @noinspection PhpMissingParamTypeInspection
     */
    final public function handle($payload): void
    {
        /*
         * get handler and be sure whether all is ok.
         * try-catch make exception\errors is silently.
         */
        try {
            /** @var HandlerInterface */
            $handler = $this->getConnectionManager()
                ->getResolverAliasHandler()
                ->resolveHandlerByRouteAlias($payload->getPayloadAlias());
        } catch (Exception $exception) {
            $this->processException($exception);
            /* break  whether handler not found */
            foreach ($this->getRepository()->getAll($payload->getPayloadAlias()) as $pid) {
                $this->killProcess($pid);
            }

            $this->getRepository()->removeAll($payload->getPayloadAlias());
            return;
        }

        /* break whether it is ok and equal */
        if ($this->getRepository()->count($payload->getPayloadAlias()) === $payload->getPayloadNumber()) {
            return;
        }

        /* kill some extra */
        if ($this->getRepository()->count($payload->getPayloadAlias()) > $payload->getPayloadNumber()) {
            $i = $this->getRepository()->count($payload->getPayloadAlias()) - $payload->getPayloadNumber();
            for (; $i--; ) {
                $pid = $this->getRepository()->getOne($payload->getPayloadAlias());
                $this->killProcess($pid);
                $this->getRepository()->remove($payload->getPayloadAlias(), $pid);
            }

            return;
        }

        /*
         * forge some new
         * $this->getRepository()->count($payload->getPayloadAlias()) < $payload->getPayloadNumber()
         */
        $i = $payload->getPayloadNumber() - $this->getRepository()->count($payload->getPayloadAlias());
        for (; $i--;) {
            $this->getRepository()->add($payload->getPayloadAlias(), $this->makeNewProcess($handler));
        }
    }

    /** @inheritdoc  */
    public function getMessageAllowedClasses(): array
    {
        return [ ProcessManagerMessage::class ];
    }

    /**
     * Method for processing exceptions.
     *
     * Override to implement any action.
     *
     * @param Exception $exception
     */
    public function processException(Exception $exception): void
    {
    }
}
