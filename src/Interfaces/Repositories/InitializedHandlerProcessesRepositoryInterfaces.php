<?php

namespace Planet17\MessageQueueProcessManager\Interfaces\Repositories;

/**
 * Interface InitializedHandlersRepositoryInterfaces
 *
 * @package Planet17\MessageQueueProcessManager\Interfaces\Repositories
 */
interface InitializedHandlerProcessesRepositoryInterfaces
{
    /**
     * Add `pid` to concrete `alias`.
     *
     * @param $alias
     * @param $pid
     *
     * @return InitializedHandlerProcessesRepositoryInterfaces
     */
    public function add($alias, $pid): InitializedHandlerProcessesRepositoryInterfaces;

    /**
     * Get all `pid`'s by alias.
     *
     * @param $alias
     *
     * @return array
     */
    public function getAll($alias): array;

    /**
     * Get first one `pid` by alias.
     *
     * @param $alias
     *
     * @return int|null
     */
    public function getOne($alias): ?int;

    /**
     * Remove concrete `pid` from alias.
     *
     * @param $alias
     * @param $pid
     *
     * @return InitializedHandlerProcessesRepositoryInterfaces
     */
    public function remove($alias, $pid): InitializedHandlerProcessesRepositoryInterfaces;

    /**
     * Remove all `pid` from alias.
     *
     * @param $alias
     *
     * @return InitializedHandlerProcessesRepositoryInterfaces
     */
    public function removeAll($alias): InitializedHandlerProcessesRepositoryInterfaces;

    /**
     * Count `pid`'s by alias.
     *
     * @param $alias
     *
     * @return int
     */
    public function count($alias): int;
}
